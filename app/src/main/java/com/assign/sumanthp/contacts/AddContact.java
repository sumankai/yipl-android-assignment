package com.assign.sumanthp.contacts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class AddContact extends AppCompatActivity {

    EditText fNameEt, lNameEt,addressEt,phoneEt;
    DatabaseReference mDatabase;
    Button saveContact;
    ArrayList<Users> contactList = new ArrayList<>();
    Toolbar toolbar;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference("Contacts");
        saveContact = (Button)findViewById(R.id.saveButton);
        fNameEt = (EditText) findViewById(R.id.firstName);
        lNameEt = (EditText) findViewById(R.id.lastName);
        addressEt = (EditText) findViewById(R.id.address);
        phoneEt = (EditText) findViewById(R.id.phoneNo);


        //Saving Contact
        saveContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fieldEmpty();   // Checking if fields are empty
                if (!fieldEmpty()) {
                    addContacts();
                    Toast.makeText(getApplicationContext(), "Contact Saved", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

    }

    //VAlidating The Fields for checking whether the fileds are empty or not
    private boolean fieldEmpty() {
        boolean f = false;
        boolean l = false;
        boolean a = false;
        boolean p = false;
        if (fNameEt.getText().toString().trim().isEmpty()) {
            fNameEt.setError("Field Cannot be Empty");
            f = true;
        }
        if (lNameEt.getText().toString().trim().isEmpty()) {
            lNameEt.setError("Field Cannot be Empty");
            l = true;
        }
        if (addressEt.getText().toString().trim().isEmpty()) {
            addressEt.setError("Field Cannot be Empty");
            a = true;
        }
        if (phoneEt.getText().toString().trim().isEmpty()) {
            phoneEt.setError("Field Cannot be Empty");
            p = true;
        }

        if (f || l || a || p){
            return true;
        }
        else{
            return false;
        }
    }


    //Method For Adding Contacts
    private void addContacts() {
        String first,last,address,phone;

        first = fNameEt.getText().toString().trim();
        last = lNameEt.getText().toString().trim();
        address = addressEt.getText().toString().trim();
        phone = phoneEt.getText().toString().trim();
        //Saving Contacts to Firebase
        String id = mDatabase.push().getKey();
        Users user = new Users(first,last,address,phone,id,"false");
        mDatabase.child(id).setValue(user);

    }
}
