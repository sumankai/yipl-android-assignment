package com.assign.sumanthp.contacts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by SumanThp on 11/9/2017.
 */

public class Favorites extends Fragment {

    RecyclerView rv;
    DatabaseReference databaseReferences;
    ArrayList<Users> contactList = new ArrayList<>();
    DividerItemDecoration dividerItemDecoration;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favorites_user, container, false); // Creates View For the Favorites

        databaseReferences = FirebaseDatabase.getInstance().getReference("Contacts");

        rv = (RecyclerView) v.findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        favoriteContacts();

        return v;
    }

    private void favoriteContacts() {
        databaseReferences.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                contactList.clear();
                for (DataSnapshot contactSnapshot : dataSnapshot.getChildren()) {
                    Users user = contactSnapshot.getValue(Users.class);
                    if (user.getFavorites().equals("true")) {                           // List only the Favorites data from firebase database
                        contactList.add(user);
                    }
                }
                dividerItemDecoration = new DividerItemDecoration(rv.getContext(), DividerItemDecoration.VERTICAL);
                rv.addItemDecoration(dividerItemDecoration);
                rv.setAdapter(new FavoriteAdapter(getActivity(), contactList));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private class FavoriteAdapter extends RecyclerView.Adapter<ViewHolder> {
        ArrayList<Users> contactList = new ArrayList<>();
        Context ctx;

        public FavoriteAdapter(Context ctx, ArrayList<Users> contactList) {
            this.ctx = ctx;
            this.contactList = contactList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(ctx).inflate(R.layout.favorites_user_row, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            Users user = contactList.get(position);

            holder.CONTACT_NAME.setText(user.getFirstname());
            holder.CONTACT_PHONE.setText(user.getPhone());


        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView CONTACT_NAME;
        TextView CONTACT_PHONE;


        public ViewHolder(View itemView) {
            super(itemView);

            CONTACT_NAME = (TextView) itemView.findViewById(R.id.nameTextView);
            CONTACT_PHONE = (TextView) itemView.findViewById(R.id.phoneTextView);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Users user = contactList.get(getAdapterPosition());
                    //Intent intent = (new Intent(Contacts.this, UserProfile.class));
                    Intent intent = (new Intent(getActivity(), UserProfile.class));
                    intent.putExtra("id", user.getUserid());
                    intent.putExtra("fname", user.getFirstname());
                    intent.putExtra("lname", user.getLastname());
                    intent.putExtra("address", user.getAddress());
                    intent.putExtra("phone", user.getPhone());
                    startActivity(intent);
                }
            });*/
        }
    }
}
