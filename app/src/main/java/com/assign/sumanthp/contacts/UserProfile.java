package com.assign.sumanthp.contacts;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by SumanThp on 11/8/2017.
 */

public class UserProfile extends AppCompatActivity {
    Bundle profileData;
    String Fname, Lname, Add, Phone, ID;
    TextView fnametv, lnametv, addtv, phonetv;
    Toolbar toolbar;
    FloatingActionButton editFab;
    String favorites;
    DatabaseReference databaseReference;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contact_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //Deleting Contact
        if (item.getItemId() == R.id.delete_contact) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Delete")
                    .setMessage("Are You sure")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseReference.child(ID).removeValue();
                            Toast.makeText(getApplicationContext(), "Contact Deleted", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setCancelable(true)
                    .setIcon(android.R.drawable.ic_input_delete)
                    .show();

        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);

        databaseReference = FirebaseDatabase.getInstance().getReference("Contacts");
        editFab = (FloatingActionButton) findViewById(R.id.editProfileFab);
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        profileData = getIntent().getExtras(); // Getting data from prev activity
        //Getting values from intent
        Fname = profileData.getString("fname");
        Lname = profileData.getString("lname");
        Add = profileData.getString("address");
        Phone = profileData.getString("phone");
        ID = profileData.getString("id");
        favorites = profileData.getString("favorites");

        fnametv = (TextView) findViewById(R.id.fViewName);
        lnametv = (TextView) findViewById(R.id.lViewName);
        addtv = (TextView) findViewById(R.id.addViewName);
        phonetv = (TextView) findViewById(R.id.phoneViewName);

        //Setting The user profile Data
        displayProfile();
        //Edit the profile
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editIntent = new Intent(UserProfile.this, UpdateContact.class);
                editIntent.putExtra("firstname", Fname);
                editIntent.putExtra("lastname", Lname);
                editIntent.putExtra("address", Add);
                editIntent.putExtra("phone", Phone);
                editIntent.putExtra("id", ID);
                editIntent.putExtra("Fav", favorites);
                startActivity(editIntent);
            }
        });
    }

    private void displayProfile() {
        fnametv.setText(Fname);
        lnametv.setText(Lname);
        addtv.setText(Add);
        phonetv.setText(Phone);
    }
}