package com.assign.sumanthp.contacts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SumanThp on 11/7/2017.
 */

public class Contacts extends Fragment {

    RecyclerView rv;
    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Users> contactList = new ArrayList<>();
    DatabaseReference databaseReferences;
    DividerItemDecoration dividerItemDecoration;
    FloatingActionButton fab;
    int addFavOff = R.drawable.ic_new_star_off;
    int addFavOn = android.R.drawable.star_big_on;

    boolean favOn = false;  //For Checking status of favorites

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.contacts_main_activity, container, false); // Creating view for the Contacts Home

        databaseReferences = FirebaseDatabase.getInstance().getReference("Contacts");

        rv = (RecyclerView) view.findViewById(R.id.recyclerView);
        fab = (FloatingActionButton) view.findViewById(R.id.addFab);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);

        handleScrollFab();


        displayContacts();

        //Adding Contacts On Clicking Fab

        AddContacts();

        return view;
    }

    private void AddContacts() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddContact.class);
                startActivity(intent);
            }
        });
    }

    private void handleScrollFab() {
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    Toast.makeText(getActivity(), "a" + fab.isShown(), Toast.LENGTH_SHORT).show();
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                } else if (dy < 0) {
                    Toast.makeText(getActivity(), "b" + fab.isShown(), Toast.LENGTH_SHORT).show();
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });
    }

   /* @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_main_activity);

        databaseReferences = FirebaseDatabase.getInstance().getReference("Contacts");
        rv = (RecyclerView) findViewById(R.id.recyclerView);
        fab = (FloatingActionButton) findViewById(R.id.addFab);
        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        sharedPreferences = getSharedPreferences("contacts", MODE_PRIVATE);
        editor = getSharedPreferences("contacts", MODE_PRIVATE).edit();

        toolbar = (Toolbar) findViewById(R.id.myToolbar); //Custom toolbar
        setSupportActionBar(toolbar);
        displayContacts();



    }*/

    private void displayContacts() {
        databaseReferences.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                contactList.clear();
                for (DataSnapshot contactSnapshot : dataSnapshot.getChildren()) {
                    Users user = contactSnapshot.getValue(Users.class);
                    contactList.add(user);
                }
                dividerItemDecoration = new DividerItemDecoration(rv.getContext(), DividerItemDecoration.VERTICAL);
                rv.addItemDecoration(dividerItemDecoration);
                rv.setAdapter(new ContactAdapter(getActivity(), contactList));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

// Recylerview Custom Adapter
    private class ContactAdapter extends RecyclerView.Adapter<ViewHolder> {
        ArrayList<Users> contactList = new ArrayList<>();
        Context ctx;

        public ContactAdapter(Context ctx, ArrayList<Users> contactList) {
            this.ctx = ctx;
            this.contactList = contactList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(ctx).inflate(R.layout.contacts_row, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            final Users user = contactList.get(position);
            holder.CONTACT_NAME.setText(user.getFirstname());
            holder.CONTACT_PHONE.setText(user.getPhone());




            if ( user.getFavorites().equals("true")) {
                favOn = true;
                holder.favButton.setImageResource(addFavOn);

            } else {
                favOn = false;
                holder.favButton.setImageResource(addFavOff);

            }

            //Adding / Removing to / from  Favorites
            holder.favButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (favOn) {
                        favOn = false;
                        holder.favButton.setImageResource(addFavOff);
                        databaseReferences.child(user.getUserid()).child("favorites").setValue("false");

                    } else {
                        favOn = true;
                        holder.favButton.setImageResource(addFavOn);
                        databaseReferences.child(user.getUserid()).child("favorites").setValue("true");

                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView CONTACT_NAME;
        TextView CONTACT_PHONE;
        ImageButton favButton;


        public ViewHolder(View itemView) {
            super(itemView);

            CONTACT_NAME = (TextView) itemView.findViewById(R.id.nameTextView);
            CONTACT_PHONE = (TextView) itemView.findViewById(R.id.phoneTextView);
            favButton = (ImageButton) itemView.findViewById(R.id.addFav);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Users user = contactList.get(getAdapterPosition());
                    //Intent intent = (new Intent(Contacts.this, UserProfile.class));
                    Intent intent = (new Intent(getActivity(), UserProfile.class));
                    intent.putExtra("id", user.getUserid());
                    intent.putExtra("fname", user.getFirstname());
                    intent.putExtra("lname", user.getLastname());
                    intent.putExtra("address", user.getAddress());
                    intent.putExtra("phone", user.getPhone());
                    intent.putExtra("favorites", user.getFavorites());
                    startActivity(intent);
                }
            });
        }
    }
}
