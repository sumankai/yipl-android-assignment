package com.assign.sumanthp.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by SumanThp on 11/9/2017.
 */

public class ContactHome extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    Toolbar toolbar;
    ViewPager mViewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_home);

        mViewPager = (ViewPager) findViewById(R.id.container);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());  // Data / Adapter for filling the tab or viewpager
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);  // Swiping Mechanics Added to Tabs

        tabLayout = (TabLayout) findViewById(R.id.tabsLayout);
        tabLayout.setupWithViewPager(mViewPager);
    }


    //Custom Class for the view pager adapter
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    Favorites favorites = new Favorites();
                    return favorites;

                case 1:
                    Contacts contacts = new Contacts();
                    return contacts;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Favorites";
                case 1:
                    return "Contacts";
            }
            return null;
        }
    }
}
