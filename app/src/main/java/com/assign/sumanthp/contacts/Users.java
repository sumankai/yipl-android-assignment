package com.assign.sumanthp.contacts;

/**
 * Created by SumanThp on 11/7/2017.
 * Class For Filling the data For / from database
 */

public class Users {

    String firstname;
    String lastname;
    String address;
    String phone;
    String userid;
    String favorites;
    public Users(){

    }
    public Users(String firstname, String lastname,String address, String phone,String userid,String favorites){
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.phone = phone;
        this.userid = userid;
        this.favorites = favorites;
    }

    public String getUserid(){
        return userid;
    }
    public String getFirstname(){
        return firstname;
    }

    public String getLastname(){
        return lastname;
    }

    public String getAddress(){
        return address;
    }

    public String getPhone(){
        return phone;
    }

    public String getFavorites(){
        return  favorites;
    }
}
