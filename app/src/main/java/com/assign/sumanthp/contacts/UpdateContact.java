package com.assign.sumanthp.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by SumanThp on 11/9/2017.
 */

public class UpdateContact extends AppCompatActivity {

    Bundle bundle;
    String fullName;
    EditText fNameEt, lNameEt, addressEt, phoneEt;
    DatabaseReference mDatabase;
    Button saveContact;
    ArrayList<Users> contactList = new ArrayList<>();
    Toolbar toolbar;
    String firstname,lastname,address,phone,ID,favorites;
    TextView toolbarText;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);

        //get String from the user profile
        bundle = getIntent().getExtras();
        firstname = bundle.getString("firstname");
        lastname = bundle.getString("lastname");
        address = bundle.getString("address");
        phone = bundle.getString("phone");
        ID = bundle.getString("id");
        favorites = bundle.getString("Fav");

        fullName = firstname + " " + lastname;
        //Custom Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        toolbarText = (TextView) toolbar.findViewById(R.id.cusToolText);
        setSupportActionBar(toolbar);
        toolbarText.setText(fullName);

        //Back navigation
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mDatabase = FirebaseDatabase.getInstance().getReference("Contacts");
        saveContact = (Button) findViewById(R.id.saveButton);
        fNameEt = (EditText) findViewById(R.id.firstName);
        lNameEt = (EditText) findViewById(R.id.lastName);
        addressEt = (EditText) findViewById(R.id.address);
        phoneEt = (EditText) findViewById(R.id.phoneNo);

        //Fill the orginal data
        DisplayData();
        //Saving Contact
        saveContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fieldEmpty();   // Checking if fields are empty
                if (!fieldEmpty()) {
                    updateContacts();
                    Toast.makeText(getApplicationContext(), "Contact Saved", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

    }

    private void DisplayData() {
        fNameEt.setText(firstname);
        lNameEt.setText(lastname);
        addressEt.setText(address);
        phoneEt.setText(phone);
    }

    private boolean fieldEmpty() {
        boolean f = false;
        boolean l = false;
        boolean a = false;
        boolean p = false;
        if (fNameEt.getText().toString().trim().isEmpty()) {
            fNameEt.setError("Field Cannot be Empty");
            f = true;
        }
        if (lNameEt.getText().toString().trim().isEmpty()) {
            lNameEt.setError("Field Cannot be Empty");
            l = true;
        }
        if (addressEt.getText().toString().trim().isEmpty()) {
            addressEt.setError("Field Cannot be Empty");
            a = true;
        }
        if (phoneEt.getText().toString().trim().isEmpty()) {
            phoneEt.setError("Field Cannot be Empty");
            p = true;
        }

        if (f || l || a || p) {
            return true;
        } else {
            return false;
        }
    }

    private void updateContacts() {
        String first, last, address, phone;

        first = fNameEt.getText().toString().trim();
        last = lNameEt.getText().toString().trim();
        address = addressEt.getText().toString().trim();
        phone = phoneEt.getText().toString().trim();
        //Saving Contacts to Firebase

        Users user = new Users(first, last, address, phone,ID,favorites);
        mDatabase.child(ID).setValue(user);

    }
}