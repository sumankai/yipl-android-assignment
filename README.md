## ContactApplication

My Application performs the basic operations like following : 

1. Create New Contact
2. Updating and Deleting
3. Adding to Favorites
4. Shows Contact details
5. Syncing new added contact in real time

## MBaaS Platform used 

- FireBase because it provides faster realtime database and greater flexibility in adding datas

## ScreenShots
- The basic application screenshots are available on source projects
